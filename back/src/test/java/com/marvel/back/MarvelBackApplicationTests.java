package com.marvel.back;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
class MarvelBackApplicationTests {

	
	
	@Test
	void contextLoads() {
		
		
	}
	
	
	@Test
	void create_new_Hero_from_JSON() throws JSONException, Exception, JsonProcessingException {
		
			JSONObject test = new JSONObject()
						.put("results", new JSONArray()
							.put(new JSONObject()
								.put("id", 1011334)
								.put("name", "3-D Man")
								.put("description", "")
								.put("thumbnail", new JSONObject()
									.put("path","http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784")
									.put("extension", "jpg")
									)
								)
						);
			
			Map<String, Object> map = new ObjectMapper().readValue(test.toString(), new TypeReference<Map<String, Object>>() {});
			
			Heroes heroes = new Heroes(map);
			Hero hero = new Hero(heroes.values.get(0));
			assertEquals("3-D Man", hero.name);
			assertEquals("", hero.description);
			
		
	}
	
	

}
