package com.marvel.back;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Heroes {
	public List<Hero> values;

    @JsonProperty("data")
    private void unpackNested(Map<String,Object> data) {
    	ArrayList results = (ArrayList)data.get("results");

    	ObjectMapper mapper = new ObjectMapper();

    	this.values = Arrays.stream(results.toArray())
		  .map(object -> mapper.convertValue(object, Hero.class))
		  .collect(Collectors.toList());        
    }
    
    public Heroes(Map<String,Object> data) {
    	this.unpackNested(data);
    }
}
