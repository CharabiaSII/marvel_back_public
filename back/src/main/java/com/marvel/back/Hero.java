package com.marvel.back;


import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Hero {

	@Getter
	@Setter
	@JsonProperty("id")
	public int id;

	@Getter
	@Setter
	@JsonProperty("name")
	public String name;

	@Getter
	@Setter
	@JsonProperty("description")
	public String description;

	@Getter
	@Setter
	public String path;

	@Getter
	@Setter
	public String extension;

    @JsonProperty("thumbnail")
    private void unpackNested(Map<String,Object> data) {
    	this.path = (String)data.get("path");
    	this.extension = (String)data.get("extension");
    }
    
    public Hero(Hero hero) {
    	this.id = hero.id;
    	this.name = hero.name;
    	this.description = hero.description;
    	this.path = hero.path;
    	this.extension = hero.extension;
    }
}
