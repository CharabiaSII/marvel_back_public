package com.marvel.back.Favorites;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "favorites")
@Data
public class Favorites {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="idfavorites")
  private int idfavorites;

  @Column(name="iduser")
  private int iduser;

  @Column(name="idhero")
  private int idhero;

  public Favorites(int iduser, int idhero) {
	super();
	this.iduser = iduser;
	this.idhero = idhero;
  }
  
  
}