package com.marvel.back.Favorites;

public class FavoritesErrorCreation extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FavoritesErrorCreation(){
		super("Favorites already created");
	}

}
