package com.marvel.back.Favorites;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.Data;

@Data
@Service
public class FavoritesService {
	
	  @Autowired
	    private FavoritesRepo favRepo;

	    public Optional<Favorites> getFav(final int id) {
	        return favRepo.findById(id);
	    }

	    public Iterable<Favorites> getAll() {
	        return favRepo.findAll();
	    }
	    
	    public ResponseEntity<List<Integer>> getByUser(final int id){
	    	return new ResponseEntity<>((ArrayList<Integer>) favRepo.findAllFavoritesByUser(id), HttpStatus.OK);
	    }
	    
	    public ResponseEntity<Favorites> saveOne (Favorites fav) throws FavoritesErrorCreation {
	    	if(favRepo.exist(fav.getIduser(), fav.getIdhero()).isPresent()) {
	    		throw new FavoritesErrorCreation();
	    	}
	    	return new ResponseEntity<>(favRepo.save(fav), HttpStatus.OK);
	    }

		public ResponseEntity<Void> deleteFav(Favorites fav) throws FavoritesNotFoundException {
			Optional<Integer> idTemp = favRepo.exist(fav.getIduser(), fav.getIdhero());
			if (idTemp.isPresent()) {
				favRepo.deleteById(idTemp.get());
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				throw new FavoritesNotFoundException();
			}
		}

}
