package com.marvel.back.Favorites;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class FavoritesErrorAdvice {
	
	@ResponseBody
	@ExceptionHandler(FavoritesErrorCreation.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String favoritesNotCreatedHandler(FavoritesErrorCreation error) {
		return error.getMessage();
	}
	
	@ResponseBody
	@ExceptionHandler(FavoritesNotFoundException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String favoritesNotFoundHandler(FavoritesNotFoundException error) {
		return error.getMessage();
	}
	
	

}
