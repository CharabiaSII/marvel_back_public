package com.marvel.back.Favorites;

public class FavoritesNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FavoritesNotFoundException(){
		super ("Could not find favorites ");
	}

}
