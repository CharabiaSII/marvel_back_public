package com.marvel.back.Favorites;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FavoritesRepo extends JpaRepository<Favorites, Integer> {
	
	@Query(value = "SELECT idhero FROM favorites f WHERE f.iduser = :iduser", 
			  nativeQuery = true)
	Iterable<Integer> findAllFavoritesByUser(@Param("iduser") Integer iduser);
	
	@Query(value = "SELECT idfavorites FROM favorites f WHERE f.iduser = :iduser AND f.idhero = :idhero", 
			  nativeQuery = true)
	Optional<Integer> exist(@Param("iduser") Integer iduser, @Param("idhero") Integer idhero);
	

}