package com.marvel.back.Favorites;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@RequestMapping(path="/demo") 
public class FavoritesController {
  @Autowired 
  private FavoritesService favService;

  @GetMapping(path="/1")
  public Optional<Favorites> getOne() {
    return favService.getFav(1);
  }
  
  @GetMapping(path="/all")
  public Iterable<Favorites> getAll() {
    return favService.getAll();
  }
  
  @GetMapping(path="/user/{id}")
  public ResponseEntity<List<Integer>> getAllFavoritesByUser(@PathVariable int id) {
    return favService.getByUser(id);
  }
  
  @PostMapping(path="/addfav")
  public ResponseEntity<Favorites> createFavorites(@RequestBody Favorites fav) throws FavoritesErrorCreation{
	  return favService.saveOne(fav);
  }
  
  @DeleteMapping(path="/removefav")
  public ResponseEntity<Void> removeFavorites(@RequestBody Favorites fav) throws FavoritesNotFoundException {
	  return favService.deleteFav(fav);
  }
  
  @GetMapping(path="/test")
  public String hello() {
	  var test = "hello";
    return test;
  }
  
  
}