package com.marvel.back;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

import lombok.Getter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class HeroService {
	
	static final String URL_BASE = "https://gateway.marvel.com:443/v1/public/characters";
		
	@Value("${apikey}")
	@Getter
	private String apikey;
	
	@Value("${privateKey}")
	@Getter
	private String privateKey;
	
	@Autowired
	WebClient webClient;
	
	public Mono<Heroes> findAll()
	{
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
		String ts = formatter.format(date); 
		String hash = Util.getMd5(ts + privateKey + apikey);

		ResponseSpec query = webClient.get()
				.uri(uriBuilder -> uriBuilder
					    .path("/characters")
					    .queryParam("ts", ts)
					    .queryParam("apikey", apikey)
					    .queryParam("hash", hash)
					    .queryParam("limit", 20)
					    .build())
				.retrieve();		
		
		return query.bodyToMono(Heroes.class);
	}
	
	public Mono<Heroes> findAll(int page)
	{
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
		String ts = formatter.format(date); 
		String hash = Util.getMd5(ts + privateKey + apikey);

		ResponseSpec query = webClient.get()
				.uri(uriBuilder -> uriBuilder
					    .path("/characters")
					    .queryParam("ts", ts)
					    .queryParam("apikey", apikey)
					    .queryParam("hash", hash)
					    .queryParam("limit", 20)
					    .queryParam("offset", page*20)
					    .build())
				.retrieve();		
		
		return query.bodyToMono(Heroes.class);
	}
	
	
	public Flux<Heroes> findAllById(List<Integer> idhero)
	{
		return Flux.fromIterable(idhero).flatMap(this::findById);
	}
	
	public Mono<Heroes> findById(int idhero)
	{
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
		String ts = formatter.format(date); 
		String hash = Util.getMd5(ts + privateKey + apikey);
		
		return webClient.get()
			.uri(uriBuilder -> uriBuilder
				    .path("/characters/" + idhero)
				    .queryParam("ts", ts)
				    .queryParam("apikey", apikey)
				    .queryParam("hash", hash)
				    .build())
			.retrieve()
			.bodyToMono(Heroes.class);
	}
		
}


