package com.marvel.back;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping(value = "/heros")
public class HeroController {
	
	@Autowired
    HeroService heroService;
	
	@GetMapping(path ="/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Hero> getAll(@RequestParam(required = false) Integer page) {
        try {  
        	Heroes heroes;
        	System.out.println(page);
        	if(page != null) {
        		heroes = heroService.findAll(page).block();
        	} else {
        		heroes = heroService.findAll().block();
        	}
            return heroes.values;
        }catch (Exception e){
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "HTTP Status will be NOT FOUND (CODE 404)\n");
        }
    }
	
	@GetMapping("/byHero")
    public List<Hero> getSome(@RequestParam List<Integer> idHeroes) {
        try {
        	Flux<Heroes> flux = heroService.findAllById(idHeroes);
        	List<Heroes> listheroes = flux.collectList().block();
        	List<Hero> retour = new ArrayList<Hero>();
        	for (Heroes heroes : listheroes) {
        		heroes.values.forEach(hero -> retour.add(hero));
        	}
        	
        	return retour;
        }catch (Exception e){
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "HTTP Status will be NOT FOUND (CODE 404)\n");
        }
    }
	
	@GetMapping("/byOne")
    public Mono<Heroes> getOne(@RequestParam Integer idHero) {
        try {
            return heroService.findById(idHero);
        }catch (Exception e){
            e.printStackTrace();
            return Mono.error(new Exception("server error"));
        }
    }

}
