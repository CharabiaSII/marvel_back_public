# Marvel_Back


## Configuration

- Projet Spring Boot (spring-boot-starter-parent 2.7.3)
- Java (11)
- Eclipse (4.25)
- MySQL (8)

## Dépendances

- spring-boot-starter-data-jpa
- spring-boot-starter-web
- spring-boot-starter-webflux
- spring-boot-devtools
- mysql-connector-java
- lombok


## Observations

Script SQL dans src/main/resources.

Prévoir authentification pour changer d'utilisateur.

Par défaut, utilisateur ID 1.
